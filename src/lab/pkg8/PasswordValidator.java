/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lab.pkg8;

class PasswordValidator extends Exception {
  
    int passwordConditionViolated = 0;
  
    public PasswordValidator (int conditionViolated)
    {
        super("Invalid Password: ");
        passwordConditionViolated = conditionViolated;
    }
  
    public String printMessage()
    {
        switch (passwordConditionViolated) {
  
        
        case 1:
            return ("Password length should be"
                    + " between 8 or more characters");
  
            
  
        case 2:
            return ("Password should contain"
                    + " at least one digit(0-9)");
  
        case 3:
            return ("Password should contain at "
                    + "least one special character");
  

        case 4:
            return ("Password should contain at"
                    + " least one uppercase letter(A-Z)");
  
        case 5:
            return ("Password should contain at"
                    + " least one lowercase letter(a-z)");
        }
  
        return ("");
    }
}
