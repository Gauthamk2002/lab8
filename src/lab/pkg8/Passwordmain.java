/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lab.pkg8;

/**
 *
 * @author Admin
 */
public class Passwordmain {
    // A utility function to check
    // whether a password is valid or not
    public static void isValid(String password)
        throws PasswordValidator
    {
  
        // for checking if password length
        // is between 8 and 15
        if (!((password.length() >= 8)
              && (password.length() <= 15))) {
            throw new PasswordValidator(1);
        }
  
        // to check space
        if (password.contains(" ")) {
            throw new PasswordValidator(2);
        }
        if (true) {
            int count = 0;
  
            // check digits from 0 to 9
            for (int i = 0; i <= 9; i++) {
  
                // to convert int to string
                String str1 = Integer.toString(i);
  
                if (password.contains(str1)) {
                    count = 1;
                }
            }
            if (count == 0) {
                throw new PasswordValidator(3);
            }
        }
  
        // for special characters
        if (!(password.contains("@") || password.contains("#")
              || password.contains("!") || password.contains("~")
              || password.contains("$") || password.contains("%")
              || password.contains("^") || password.contains("&")
              || password.contains("*") || password.contains("(")
              || password.contains(")") || password.contains("-")
              || password.contains("+") || password.contains("/")
              || password.contains(":") || password.contains(".")
              || password.contains(", ") || password.contains("<")
              || password.contains(">") || password.contains("?")
              || password.contains("|"))) {
            throw new PasswordValidator(4);
        }
  
        if (true) {
            int count = 0;
  
            // checking capital letters
            for (int i = 65; i <= 90; i++) {
  
                // type casting
                char c = (char)i;
  
                String str1 = Character.toString(c);
                if (password.contains(str1)) {
                    count = 1;
                }
            }
            if (count == 0) {
                throw new PasswordValidator(5);
            }
        }
  
        if (true) {
            int count = 0;
  
            // checking small letters
            for (int i = 90; i <= 122; i++) {
  
                // type casting
                char c = (char)i;
                String str1 = Character.toString(c);
  
                if (password.contains(str1)) {
                    count = 1;
                }
            }
            if (count == 0) {
                throw new PasswordValidator(6);
            }
        }
  
        // The password is valid
    }
  
    // Driver code
    public static void main(String[] args)
    {
  
        String password1 = "Gauthamkk";
  
        try {
            System.out.println("Is Password "
                               + password1 + " valid?");
            isValid(password1);
            System.out.println("Valid Password");
        }
        catch (PasswordValidator e) {
            System.out.print(e.getMessage());
            System.out.println(e.printMessage());
        }
  
        String password2 = "Gauthamkk@123";
        try {
            System.out.println("\nIs Password "
                               + password2 + " valid?");
            isValid(password2);
            System.out.println("Valid Password");
        }
        catch (PasswordValidator e) {
            System.out.print(e.getMessage());
            System.out.println(e.printMessage());
        }
    }

    
}
